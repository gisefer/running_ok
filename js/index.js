$(function(){
    $("[data-toggle='tooltip']").tooltip();
    $("[data-toggle='popover']").popover();
    $('.carousel').carousel({
        interval:3000
    });
    $('.alert').alert();
    $('#contacto').on('show.bs.modal', function(e){
        console.log('mostrando modal');
        $('#contactobtn').removeClass('btn-outline-success');
        $('#contactobtn').addClass('btn-primary');
        $('#contactobtn').prop('disabled',true);
        
    });
    $('#contacto').on('shown.bs.modal', function(e){
        console.log('el modal  se mostró');
    });
    $('#contacto').on('hide.bs.modal', function(e){
        console.log('ocultando modal');
    });
    $('#contacto').on('hidden.bs.modal', function(e){
        console.log('el modal  se ocultó');
        $('#contactobtn').prop('disabled',false);
        $('#contactobtn').removeClass('btn-primary');
        $('#contactobtn').addClass('btn-outline-success');
    });
    $('.grid').masonry({
        itemSelector: '.grid-item',
        columnWidth: '.grid-sizer',
        percentPosition: true
        });
});

